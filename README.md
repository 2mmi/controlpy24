# README #

This is researchware code - not ready for production..

There is a lot of "magic-numbers" in the code - will clean this up over time.

Right now its working with Cubase (with the included template)

 - All faderes - (2 way communication) 

 - VU meters (but the data output from Cubase seems useless)

 - PAN (2 way communication, but needs midiloopback)

I use loopMIDI as virtual midi cables (maybe its possible to create this in python)

![midi.PNG](https://bitbucket.org/repo/6pXdan/images/1764987648-midi.PNG)


# **Dependencies** #

1  https://github.com/nmap/npcap/releases/tag/v0.80

2  PIP install pip install python-rtmidi

3  loopMIDI (virtual midi cable)


# Video time !!! #


https://www.youtube.com/watch?v=in9JskRsqxk

https://www.youtube.com/watch?v=lJDrzHe-S6k